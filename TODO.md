# Itches

Things that I would like to do or implement at some point, because I could use
them.

---

- [X] My basic (whitespace handling) vimrc implemented in the home-manager config
- [X] In `/etc/nixos`: Variable to switch between fully fledged desktop setup and
  headless setup
- [X] Switch to i3-gaps (`services.xserver.windowManager.i3.package`)
- [ ] integrate live iso into the flake https://hoverbear.org/blog/nix-flake-live-media/
- [X] Use librewolf instead of firefox
