#!/usr/bin/env bash
#
host="live"
flake_target="nixosConfigurations.${host}.config.system.build.isoImage"

echo "Building Iso Image for '${host}'..."
# create ISO image
nix build ./config/#${flake_target}
