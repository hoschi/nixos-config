# NixOS-config

My configuration files for the NixOS Linux distribution.

## How to install

### via NixOS minimal ISO

Let me note down how I would go about installing my personal setup.

- set keyboard layout `$ sudo loadkeys de`
- download this repo in a `$ nix-shell -p git` with `$ git clone` to
  `$HOME/Repos/`
- run `$ ./install system`
- reboot and login to default user
- run `$ ./install user`
