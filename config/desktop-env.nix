{ config, pkgs, ... }:

let
  # possible values with `builtins.attrNames pkgs.nixos-artwork.wallpapers`
  background="nineish";
  backgroundPath=pkgs.nixos-artwork.wallpapers.${background}.gnomeFilePath;
in
{
  # Enable the X11 windowing system.
  # Use i3wm with XFCE
  # as described at https://nixos.wiki/wiki/Xfce#Using_as_a_desktop_manager_and_not_a_window_manager
  services.xserver = {
    enable = true;

    displayManager.lightdm = {
      enable = true;
      background = backgroundPath;
    };
    
    displayManager.defaultSession = "xfce+i3";
    
    desktopManager = {
      xterm.enable = false;
      
      xfce = {
        enable = true;
        noDesktop = true;
        enableXfwm = false;
      };
    };
    windowManager.i3 = {
      enable = true;
      package = pkgs.i3-gaps;
      extraPackages = with pkgs; [ rofi i3lock i3status dmenu feh ];
      extraSessionCommands="feh --bg-scale ${backgroundPath}";
    };
    windowManager.bspwm.enable = true;
  };

}
