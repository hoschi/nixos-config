# Common configuration for headless machines (e.g., Amazon EC2
# instances).

{ lib, ... }:

with lib;

{
  boot = {
    # Since we can't manually respond to a panic, just reboot.
    kernelParams = [ "panic=1" "boot.panic_on_fail" ];

    vesa = false;

    # Being headless, we don't need a GRUB splash image.
    loader.grub.splashImage = null;
  };

  services.xserver.enable = false;

  # Don't start a tty on the serial consoles.
  systemd.services = {
    "serial-getty@ttyS0".enable = lib.mkDefault false;
    "serial-getty@hvc0".enable = false;
    "getty@tty1".enable = false;
    "autovt@".enable = false;
  };

  # Don't allow emergency mode, because we don't have a console.
  systemd.enableEmergencyMode = false;
}
