{ config, pkgs, ... }:

{
  # Set your time zone.
  time.timeZone = "Europe/Berlin";

  # Select internationalisation properties.
  i18n.defaultLocale = "de_DE.UTF-8";

  console.keyMap = "de";

  services.xserver = {
    # keymap
    layout = "de";
    xkbVariant = "";
  };
}
