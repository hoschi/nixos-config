# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, specialArgs, ... }:

let
  is_headless = (if specialArgs ? "is_headless"
    then specialArgs.is_headless
    else true);

  load_default = [
      ./localisation.nix
      ./network.nix
      ./ssh.nix
  ];
  load_headless = [ ./headless.nix ];
  load_headed = [ ./desktop-env.nix ./input-output.nix ];
in
{
  imports = load_default ++ 
    (if is_headless then load_headless else load_headed);

  nix = {
    package = pkgs.nixFlakes;
    extraOptions = ''
    experimental-features = nix-command flakes
    '';

    # garbage collection
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 14d";
      # store last gc time to disk and catch up on missed rounds
      persistent = true;
    };
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = false;

  boot.cleanTmpDir = true;


  # Define a user account. Don't forget to change the password with ‘passwd’.
  users.users.hoschi = {
    isNormalUser = true;
    description = "Hoschi";
    initialPassword = "hoschi";
    extraGroups = [ 
      "networkmanager"
      "wheel"
      "vboxsf" # allow access to virtualbox specific media
      "mlocate"
      "hoschi"
    ];
    openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC8BgmURAMbQCkhpwtwcPujho9ndgkEGfOZ5jZ9/jDbxHBPp3tkKN57p21NMktuGqXkRqkPch3Mj2udbTgDokO4UuACbRTNdDs/r5WPRAh7zBXIzM/49myHYtq4n7zYTU7YcQrTmmHQfY7raYYJW0YY/TXKVQUqhq0NhW52hOwFnmG2ZYmxnM10iqMrJghnBU6027MMuL5hHeRKqOUKAHbImApprT7vaUVdj14FtdZ7LooR7tCgeT6TjSahvIMadfRo39YXUUfv8M9oG8yj41Mw2qCXsrkQ9ex097wzRlClkX3REdiLZfn1IuENo3HmhY+XIiL190G4OCqMY/R+QUCa5t0kxHXfW3jfcSnZCOKHNO9SK7R0vXQqqd8vW5QV2b6eVYCvOmJrOhls802xfrXeExDCjM/lPdVtBvRUHTtHxbwBgPMAGIfKtk6qONMYa4I4dInacjZgHvpAVSwt74AuFBpNWfL64Ps+P//UPHQDGBHxdS2biehykclnpZ3V4/IvwDYv6xXU8VOBt8LZCFEcN4RUIKFQNm80/31eInxg7xieNJvkdbq6/6F0LjHpB0OJhujlsC7m7P5V6Wjij29elTgndQ1mNK6JTSpWgiT3Xry7T57L/Hcjes4RIjXClhGc0tzRmzUkGhNsJkKIl3YBsWYHN2+7CtsU/tP12wt2hQ==" # rsa2@otrs.com

      # janina@JsTux3
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDKK3ImpCXe4ku+RazaWdtwJW15Q59QnhC/FZBWtxCu2xjs+sFAZw91eyTyUx1q0+nm+5hwdxvRVtvBhlT5jkVJiU+pS/9qU5isI9uJPLQCVJWJx2qSGvOHcZxxiLzMmpOrWKChuLSVoOnT1FaoNErCl6FjZxiWMrVpltfD4BiI61UzhVN20Nhfocy8/w9pkVoEebSEUxTdj56cb+nn5BntLuHe6ShDaRVQJJ47z2v6UtWn7c+XzeMcVIAwlBrXrw/xjG6RqGlx9QUdWnQdFSP5s53IcAvxXlBuHbFoGaVUPRScKMb+85dISQs31LaRvGiz9vSYEItPIitq3LpuL3agp1QmOpvIwYGKNxXxOiF20U/f66KDWP6YFXJaXMUPOwxmP5ZVsnLLNJLY3iZ6hDscF/zSTfSRjFt/aXA2I/LZvITWGSsDVJCoqL3+pPczYqfuv23G1a7JkuOMN+wAoo7HPOy9tjooHAVSXY11o6Eb5glT/su3m+hASKtJvvd9zh2CGZaThTXQj6t1lG6JXeFmC5CzsWGMUECWAuS9y1bEGItDLt/bSxK7yvwoqnvZqmi3Z88vw0sDFz9GbZvlWLAbBzsYWrbcCV9Mw+JpG9Ip/eW6FB96g3EEAk3ZcuIRlXh9DweYKbLYsn9b4qRRSwV6etvuuxrfhPDjVuxc71Fm0w=="
    ];
  };


  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    # terminal
    curl
    wget
    git
    tmux
    w3m
    # editors
    vim nano
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:
  # ...

  services.locate = {
      enable = true;
      locate = pkgs.mlocate;
      interval = "hourly";
      localuser = null;
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05"; # Did you read the comment?

}
