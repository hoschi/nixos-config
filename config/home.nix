{ config, pkgs, ... }:

# Available options: https://nix-community.github.io/home-manager/options.html

let
  user = "hoschi";
  mail = "me@hoschi-it.de";
in {
  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = user;
  home.homeDirectory = "/home/${user}";

  home.packages = with pkgs; [
    # daily drivers
    thunderbird
    mupdf
    mpv
    geany
    flameshot
    fdupes
    htop

    # hardware checks
    memtester

    # rescue
    gparted
    ddrescue
    testdisk
    foremost # forensic data recovery
    safecopy
    extundelete
    ext4magic

    # backups
    fsarchiver
    rsync

  ];

  home.shellAliases = {
    weather = "curl -fGsS -H 'Accept-Language: de' --compressed 'wttr.in/?1mF'";
    l = "ls -p -F";
    ll = "ls -alh";
  };

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "22.05";

  # Let Home Manager install and manage itself.
  programs.home-manager = {
      enable = true;
      # try to not use channels, see
      # https://rycee.gitlab.io/home-manager/index.html#sec-install-standalone
      # the paragraph before the last paragraph
      path = "https://github.com/nix-community/home-manager/archive/master.tar.gz";
  };

  # programs.bash = {
  #  enable = true;
  #  historyFileSize = 100;
  #  historyIgnore = ["ls" "cd" "exit"];

  #  sessionVariables = {
  #    EDITOR = "vim";
  #  };
  #};

  programs.git = {
    userName = user;
    userEmail = mail;
    aliases = {
      "s" = "status --short";
    };
    ignores = [
      "*~"
      "*.swp"
    ];
  };

  programs.tmux = {
    enable = true;
    plugins = [ pkgs.tmuxPlugins.tmux-colors-solarized ];
    keyMode = "vi";
    clock24 = true;
  };
  
  programs.vim = {
    enable = true;

    # vimrc
    extraConfig = ''
      set nocompatible  " be iMproved
      filetype plugin indent on

      set ffs=unix,dos,mac "std file types

      " allow using the visual mouse
      set mouse=a

      " newlines and prefixed whitespace
      set textwidth=80
      set autoindent
      set smartindent
      set wrap
      set colorcolumn=80

      " tabs and spaces
      set expandtab "make tabs to spaces
      set smarttab
      set shiftwidth=4 "how many spaces are a tab
      set tabstop=4

      " UI design
      syntax enable
      set showmatch " match parenthesis
      set number relativenumber
      set ruler

      set background=dark
      colorscheme solarized " requires plugin: vim-colors-solarized
      '';

    plugins = with pkgs.vimPlugins; [
      vim-nix
      vim-gitgutter
      vim-commentary
      vim-colors-solarized
      delimitMate # auto close brackets
    ];
  };

  ###############
  #  graphical  #
  ###############

  xsession.windowManager.i3 = {
    enable = true;
    package = pkgs.i3-gaps;
    config.modifier = "Mod4";

    config.gaps = {
      inner = 20;
    };
  };

  services.sxhkd = {
    enable = true;
    keybindings = {
      "super + d" = "xfce4-appfinder";
    };
  };

  programs.firefox = {
    enable = true;
    package = pkgs.librewolf;
  };
}
