{ config, pkgs, ... }:

{

  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
    permitRootLogin = "no";
    ports = [ 712 ];
    passwordAuthentication = false;
  };

}
