{
  # Description, write anything or even nothing
  description = "Hoschis nixos setup(s)";

  # Input config, or package repos
  inputs = {
    # Nixpkgs, NixOS's official repo
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
  };

  # Output config, or config for NixOS system
  outputs = { self, nixpkgs, home-manager, ... }@inputs:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs {
        inherit system;
        config.allowUnfree = false;
      };
      lib = nixpkgs.lib;

      homemanager_modules = [
        home-manager.nixosModules.home-manager
        {
          home-manager = {
            useGlobalPkgs = true;
            useUserPackages = true;
            users.hoschi = import ./home.nix;
            # to pass arguments to home.nix:
            #extraSpecialArgs = "";
          };
        }
      ];

      configure_host = {
        hostname ? "nixos",
        is_headless ? false,
        use_homemanager ? true,
        imports ? [],
        modules ? [],
      }: nixpkgs.lib.nixosSystem {
        inherit system;
        specialArgs.is_headless = is_headless;


        modules = [
          {
            networking.hostName = hostname;
            imports = [ ./configuration.nix ] ++ imports;
          }
        ]
        ++ modules
        ++ lib.lists.optionals use_homemanager homemanager_modules;
      };
    in {
      nixosConfigurations = {
        "live" = configure_host {
          imports = [
            "${nixpkgs}/nixos/modules/installer/cd-dvd/installation-cd-minimal.nix"
          ];
          modules = [
            {
              # overwrite dvd default
              services.openssh.permitRootLogin = pkgs.lib.mkForce "no";
            }
          ];
        };

        "JsOrangeGiant" = configure_host {
          hostname = "JsOrangeGiant";
          imports = [
            ./hardware/JsOrangeGiant.nix
          ];
          modules = [{
            boot.loader.grub.device = "/dev/sda";
          }];
        };

        "nixos-vm" = configure_host {
          hostname = "nixos-vm";
          is_headless = true;
          imports = [
            ./hardware/vm.nix
          ];
          modules = [{
            boot.loader.grub.device = "/dev/sda";
          }];
        };

      };

    };

}
